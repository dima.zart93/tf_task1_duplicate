resource "aws_s3_bucket" "terraform_state" {
  bucket = "tsart-task1-state"
  force_destroy = true
  tags = {
      Name = var.name
      Namespace = var.namespace
  }  
}

resource "aws_dynamodb_table" "terraform_locks" {
  name         = "${var.name}-locks"
  billing_mode = var.billing
  hash_key     = var.hash_key
  attribute {
    name = var.hash_key
    type = "S"
  }
  tags = {
      Name = var.name
      Namespace = var.namespace
  }    
}

