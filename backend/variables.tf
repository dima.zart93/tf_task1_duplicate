variable "profile" {
    description = "Which profile to use"
    type = string
    default = "mycloud"
}

variable "region" {
    description = "Region for bucket"
    type = string
    default = "us-east-1"
}

variable "name" {
    type = string
}

variable "namespace" {
    type = string
}

variable "key" {  
    description = "Which key macth tfstate"
    type = string
    default = "terraform.tfstate"
}

variable "hash_key" {
    description = "Hash-key for DynamoDB"
    type = string
    default = "LockID"
}

variable "billing" {
    type = string
    default = "PAY_PER_REQUEST"  
}