resource "aws_instance" "task1_ec2" {
  ami           = var.OS 
  instance_type = var.instance_type
  network_interface {
    network_interface_id = aws_network_interface.task1_nic.id
    device_index         = 0
  } 
  key_name = var.key_name

  user_data = "${file("userdata.sh")}"

  tags = {
      Name = var.name
      Namespace = var.namespace
  }   
}

/**
resource "aws_network_interface_sg_attachment" "task1_sg_attachment" {
  security_group_id    = aws_security_group.task1_sg.id
  network_interface_id = aws_instance.task1_ec2.primary_network_interface_id
}

*/

