resource "aws_subnet" "task1_subnet" {
  vpc_id            = aws_vpc.task1_vpc.id
  cidr_block        = var.network
  availability_zone = var.az
  map_public_ip_on_launch = var.public_access
  tags = {
      Name = var.name
      Namespace = var.namespace
  }
}

resource "aws_network_interface" "task1_nic" {
  subnet_id   = aws_subnet.task1_subnet.id
  private_ips = var.ip_address

  tags = {
      Name = var.name
      Namespace = var.namespace
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.task1_vpc.id

  tags = {
      Name = var.name
      Namespace = var.namespace
  }
}

resource "aws_default_route_table" "task1_routes" {
  default_route_table_id = aws_vpc.task1_vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
      Name = var.name
      Namespace = var.namespace
  }
}  