output "vpc-id" {
    value = aws_vpc.task1_vpc.id
}

output "ec2-id" {
    value = aws_instance.task1_ec2.id
}
