variable "profile" {
    description = "Which profile to use"
    type = string
    default = "mycloud"
}

variable "OS" {
    description = "Which AMI to use"
    type = string
    default = "ami-04505e74c0741db8d"
}

variable "instance_type" {
    description = "Which type of instance to use"
    type = string
    default = "t2.micro"
}

variable "key_name" {
    description = "Key pair"
    type = string
    default = "Test"
}

variable "region" {
    description = "Which region to use"
    type = string
    default = "us-east-1"
}

variable "az" {
    description = "Which region to use"
    type = string
    default = "us-east-1a"
}

variable "name" {
    type = string
}

variable "namespace" {
    type = string
}

variable "cidr_block" {
    description = "Address block for VPC"
    type = string
}

variable "network" {
    description = "Network for instance"
    type = string
}

variable "ip_address" {
    description = "IP for instance"
    type = list
}

variable "public_access" {
    description = "Does it need to be public accessible?"
    type = string
    default = false
}

variable "acl" {
    description = "ACL for bucket"
    type = string
    default = "private"
}

variable "versioning" {
    description = "Enabling versioning on S3"
    type = string
    default = "Enabled"  
}