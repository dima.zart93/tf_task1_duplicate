echo "Destroying task-environment"
cd .\dev\
terraform destroy --var-file=..\tfvars\task1.tfvars

echo "Destroying tfstate-backend"
cd ..\backend\
terraform destroy --var-file=..\tfvars\backend.tfvars