echo "Deploying tfstate-backend"
cd .\backend\
terraform init
terraform apply --var-file=..\tfvars\backend.tfvars
echo "Deploying task environment"
cd ..\dev\
terraform init
terraform apply --var-file=..\tfvars\task1.tfvars
